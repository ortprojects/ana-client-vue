import { httpClient } from '../../services/api/httpClient';

const pathModule = '/ingredient-types';

const actions = {
  async fetchIngredientTypes(context) {
    try {
      const response = await httpClient.get(pathModule);
      const responseData = response.data;
      const ingredientTypes = responseData.data;
      context.commit('SET_INGREDIENT_TYPES', ingredientTypes);
    } catch (error) {
      console.log(error);
    }
  }
};

export default actions;
