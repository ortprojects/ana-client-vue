const getters = {
  ingredientTypes: state => state.ingredientTypes
};

export default getters;
