const mutations = {
  SET_INGREDIENT_TYPES: (state, payload) => {
    state.ingredientTypes = payload;
  }
};

export default mutations;
