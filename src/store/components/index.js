import Vue from 'vue';
import Vuex from 'vuex';
import snackbarModule from './snackbar';

Vue.use(Vuex);
const componentModule = {
  namespaced: true,
  modules: {
    snackbarModule
  }
};

export default componentModule;
