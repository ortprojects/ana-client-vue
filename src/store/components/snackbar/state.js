const initialState = () => ({
  isOpen: false,
  message: ''
});

const state = initialState();

export default state;
