const actions = {
  openSnackbar(context, snackbarOptions) {
    context.commit('SET_IS_OPEN', true);
    context.commit('SET_MESSAGE', snackbarOptions.message);
  },

  closeSnackbar(context) {
    context.commit('SET_IS_OPEN', false);
    context.commit('SET_MESSAGE', '');
  }
};

export default actions;
