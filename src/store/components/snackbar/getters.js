const getters = {
  isOpen: state => state.isOpen,
  message: state => state.message
};

export default getters;
