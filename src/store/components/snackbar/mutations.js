const mutations = {
  SET_IS_OPEN: (state, payload) => {
    state.isOpen = payload;
  },
  SET_MESSAGE: (state, payload) => {
    state.message = payload;
  }
};

export default mutations;
