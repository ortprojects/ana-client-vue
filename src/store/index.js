import Vue from 'vue';
import Vuex from 'vuex';
import authModule from './auth';
import hairPatternModule from './hairPatterns';
import hairTypeModule from './hairTypes';
import postModule from './posts';
import ingredientTypeModule from './ingredientTypes';
import ingredientModule from './ingredients';
import componentModule from './components';
import answerModule from './answers';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authModule: authModule,
    hairPatternModule,
    hairTypeModule,
    postModule,
    ingredientTypeModule,
    ingredientModule,
    componentModule,
    answerModule
  }
});
