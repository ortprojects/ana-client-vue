import { httpClient } from '../../services/api/httpClient';

const pathModule = '/answers';

const actions = {
  async createAnswer(context, answer) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .post(pathModule, { answer })
        .then(axiosResponse => {
          const serverResponse = axiosResponse.data;
          resolve(serverResponse);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },
  async deleteAnswer(context, answerId) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .delete(`${pathModule}/${answerId}`)
        .then(serverResponse => {
          const responseData = serverResponse.data;
          resolve(responseData);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },

  async updateAnswer(context, answer) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .put(pathModule, { answer })
        .then(axiosResponse => {
          const serverResponse = axiosResponse.data;
          resolve(serverResponse);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  }
};

export default actions;
