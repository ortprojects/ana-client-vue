const mutations = {
  SET_HAIR_PATTERNS: (state, payload) => {
    state.hairPatterns = payload;
  }
};

export default mutations;
