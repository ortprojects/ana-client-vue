import { httpClient } from '../../services/api/httpClient';

const pathModule = '/hair-patterns';

const actions = {
  async fetchHairPatterns(context) {
    try {
      const response = await httpClient.get(pathModule);
      const responseData = response.data;
      const hairPatterns = responseData.data;
      context.commit('SET_HAIR_PATTERNS', hairPatterns);
    } catch (error) {
      console.log(error);
    }
  }
};

export default actions;
