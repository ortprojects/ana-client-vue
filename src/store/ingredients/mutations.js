const mutations = {
  SET_INGREDIENTS: (state, payload) => {
    state.ingredients = payload;
  },
  SET_INGREDIENTSLIST: (state, payload) => {
    state.ingredientsList = payload;
  },
  DELETE_INGREDIENT: (state, payload) => {
    state.ingredients = payload;
  },
  SET_NOTFOUND: (state, payload) => {
    state.notFound = payload;
  }
};

export default mutations;
