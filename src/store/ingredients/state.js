const initialState = () => ({
  ingredients: [],
  ingredientsList: [],
  notFound: []
});

const state = initialState();

export default state;
