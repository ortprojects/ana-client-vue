const getters = {
  ingredients: state => state.ingredients,
  ingredientsList: state => state.ingredientsList,
  notFound: state => state.notFound
};

export default getters;
