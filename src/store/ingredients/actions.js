import { httpClient } from '../../services/api/httpClient';

const pathModule = '/ingredients';

function filterNotFound(ingredientsNames, ingredientsList) {
  const notFound = [];
  ingredientsNames.forEach(ingredientName => {
    if (
      !ingredientsList.some(ingredient => ingredient.name === ingredientName)
    ) {
      notFound.push(ingredientName);
    }
  });
  return notFound;
}
const actions = {
  async fetchIngredients(context) {
    try {
      const response = await httpClient.get(pathModule);
      const responseData = response.data;
      const ingredients = responseData.data;
      context.commit('SET_INGREDIENTS', ingredients);
    } catch (error) {
      console.log(error);
    }
  },
  async fetchIngredientsByList(context, ingredientNames) {
    try {
      const request = {
        params: {
          searchBy: ingredientNames
        }
      };
      const response = await httpClient.get(`${pathModule}/filter`, request);
      const responseData = response.data;
      const ingredientsList = responseData.data;
      const notFound = filterNotFound(ingredientNames, ingredientsList);
      context.commit('SET_NOTFOUND', notFound);
      context.commit('SET_INGREDIENTSLIST', ingredientsList);
    } catch (error) {
      console.log(error);
    }
  },
  async deleteIngredient(context, ingredientId) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .delete(`${pathModule}/${ingredientId}`)
        .then(serverResponse => {
          const responseData = serverResponse.data;
          resolve(responseData);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },
  async editIngredient(context, ingredient) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .put(`${pathModule}`, { ingredient })
        .then(serverResponse => {
          const responseData = serverResponse.data;
          resolve(responseData);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },
  async createIngredient(context, ingredient) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .post(`${pathModule}`, { ingredient })
        .then(serverResponse => {
          const responseData = serverResponse.data;
          resolve(responseData);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  }
};

export default actions;
