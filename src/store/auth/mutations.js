import { initialState } from './state';

const mutations = {
  SET_CURRENT_USER: (state, payload) => {
    state.currentUser = payload;
  },
  SET_ACCESS_TOKEN: (state, payload) => {
    state.accessToken = payload;
  },
  SET_IS_AUTHENTICATED: (state, payload) => {
    state.isAuthenticated = payload;
  },
  SET_INITIAL_STATE: state => {
    const payload = initialState();

    for (const key of Object.keys(payload)) {
      state[key] = payload[key];
    }
  }
};

export default mutations;
