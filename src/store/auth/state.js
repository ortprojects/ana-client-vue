export const initialState = () => ({
  currentUser: null,
  accessToken: '',
  isAuthenticated: false
});

const state = initialState();

export default state;
