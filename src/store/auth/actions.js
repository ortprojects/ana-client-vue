import firebase from 'firebase/app';
import { httpClient } from '../../services/api/httpClient';

const pathModule = '/users';

const actions = {
  async signUp(context, user) {
    await firebase
      .auth()
      .createUserWithEmailAndPassword(user.email, user.password);

    const promise = new Promise((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;
      user.firebaseUid = currentUser.uid;

      httpClient
        .post(`${pathModule}`, { user })
        .then(async axiosResponse => {
          const serverResponse = axiosResponse.data;
          const userResponse = serverResponse.data;
          const idToken = await currentUser.getIdToken(true); // forceRefresh

          localStorage.setItem('accessToken', idToken);
          localStorage.setItem('userRolId', userResponse.userRol.id);
          context.commit('SET_CURRENT_USER', userResponse);
          context.commit('SET_ACCESS_TOKEN', idToken);
          context.commit('SET_IS_AUTHENTICATED', true);

          resolve(userResponse);
        })
        .catch(error => {
          reject(error);
        });
    });

    return promise;
  },

  async signInWithEmailAndPassword(context, user) {
    try {
      await firebase
        .auth()
        .signInWithEmailAndPassword(user.email, user.password);

      const promise = new Promise((resolve, reject) => {
        const currentUser = firebase.auth().currentUser;
        const firebaseUid = currentUser.uid;

        httpClient
          .get(`${pathModule}/firebase/${firebaseUid}`)
          .then(async axiosResponse => {
            const serverResponse = axiosResponse.data;
            const userResponse = serverResponse.data;
            const idToken = await currentUser.getIdToken(true); // forceRefresh
            localStorage.setItem('accessToken', idToken);
            localStorage.setItem('userRolId', userResponse.userRol.id);
            context.commit('SET_CURRENT_USER', userResponse);
            context.commit('SET_ACCESS_TOKEN', idToken);
            context.commit('SET_IS_AUTHENTICATED', true);

            resolve(userResponse);
          })
          .catch(error => {
            reject(error);
          });
      });

      return promise;
    } catch (error) {
      throw new Error(error);
    }
  },
  async signInWithGooglePopUp(context) {
    const provider = new firebase.auth.GoogleAuthProvider();
    await firebase.auth().signInWithPopup(provider);

    const currentUser = firebase.auth().currentUser;
    const firebaseUid = currentUser.uid;
    const idToken = await currentUser.getIdToken(true); // forceRefresh

    httpClient
      .get(`${pathModule}/firebase/${firebaseUid}`)
      .then(async axiosResponse => {
        const serverResponse = axiosResponse.data;
        const userResponse = serverResponse.data;

        localStorage.setItem('accessToken', idToken);
        localStorage.setItem('userRolId', userResponse.userRol.id);
        context.commit('SET_CURRENT_USER', userResponse);
        context.commit('SET_ACCESS_TOKEN', idToken);
        context.commit('SET_IS_AUTHENTICATED', true);
      });
  },

  signOut(context) {
    const promise = new Promise((resolve, reject) => {
      firebase
        .auth()
        .signOut()
        .then(() => {
          context.commit('SET_INITIAL_STATE');
          localStorage.removeItem('accessToken');
          localStorage.removeItem('userRolId');
          resolve(true);
        })
        .catch(error => {
          reject(error);
        });
    });

    return promise;
  },

  async updateUser(context, user) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .put(`${pathModule}`, { user })
        .then(axiosResponse => {
          const serverResponse = axiosResponse.data;
          const userResponse = serverResponse.data;

          context.commit('SET_CURRENT_USER', userResponse);

          resolve(userResponse);
        })
        .catch(error => {
          reject(error);
        });
    });

    return promise;
  },

  async setAuthState(context, firebaseUser) {
    httpClient
      .get(`${pathModule}/firebase/${firebaseUser.uid}`)
      .then(async axiosResponse => {
        const serverResponse = axiosResponse.data;
        const userResponse = serverResponse.data;
        const idToken = await firebaseUser.getIdToken(true); // forceRefresh
        localStorage.setItem('accessToken', idToken);
        localStorage.setItem('userRolId', userResponse.userRol.id);
        context.commit('SET_CURRENT_USER', userResponse);
        context.commit('SET_ACCESS_TOKEN', idToken);
        context.commit('SET_IS_AUTHENTICATED', true);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export default actions;
