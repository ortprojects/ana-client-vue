const getters = {
  currentUser: state => state.currentUser,
  accessToken: state => state.accessToken,
  isAuthenticated: state => state.isAuthenticated
};

export default getters;
