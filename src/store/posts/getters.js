const getters = {
  posts: state => state.posts,
  postById: state => state.postById
};

export default getters;
