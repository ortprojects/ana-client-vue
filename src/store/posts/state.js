const initialState = () => ({
  posts: [],
  postById: null,
  dialog: false
});

const state = initialState();

export default state;
