const mutations = {
  SET_POSTS: (state, payload) => {
    state.posts = payload;
  },
  SET_POST_BY_ID: (state, payload) => {
    state.postById = payload;
  }
};

export default mutations;
