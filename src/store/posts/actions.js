import { httpClient } from '../../services/api/httpClient';

const pathModule = '/posts';

const actions = {
  async fetchPosts(context) {
    try {
      const response = await httpClient.get(pathModule);
      const responseData = response.data;
      const posts = responseData.data;
      context.commit('SET_POSTS', posts);
    } catch (error) {
      console.log(error);
    }
  },

  async fetchPostById(context, postId) {
    try {
      const response = await httpClient.get(pathModule + '/' + postId);
      const responseData = response.data;
      const post = responseData.data;
      context.commit('SET_POST_BY_ID', post);
    } catch (error) {
      console.log(error);
    }
  },

  async createPost(context, post) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .post(pathModule, { post })
        .then(serverResponse => {
          const responseData = serverResponse.data;
          resolve(responseData);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },

  async deletePost(context, postId) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .delete(`${pathModule}/${postId}`)
        .then(axiosResponse => {
          const serverResponse = axiosResponse.data;
          resolve(serverResponse);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  },

  async updatePost(context, post) {
    const promise = new Promise((resolve, reject) => {
      httpClient
        .put(pathModule, { post })
        .then(axiosResponse => {
          const serverResponse = axiosResponse.data;
          resolve(serverResponse);
        })
        .catch(error => {
          reject(error);
        });
    });
    return promise;
  }
};

export default actions;
