import { httpClient } from '../../services/api/httpClient';

const pathModule = '/hair-types';

const actions = {
  async fetchHairTypes(context) {
    try {
      const response = await httpClient.get(pathModule);
      const responseData = response.data;
      const hairTypes = responseData.data;
      context.commit('SET_HAIR_TYPES', hairTypes);
    } catch (error) {
      console.log(error);
    }
  }
};

export default actions;
