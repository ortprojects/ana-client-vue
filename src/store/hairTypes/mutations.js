const mutations = {
  SET_HAIR_TYPES: (state, payload) => {
    state.hairTypes = payload;
  }
};

export default mutations;
