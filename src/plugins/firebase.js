import firebase from 'firebase/app';

// Import needed firebase modules
import 'firebase/auth';

// Firebase app config
const firebaseConfig = {
  apiKey: 'AIzaSyCyYG2c4yJcekky3mpjMaDvX5w4dghf0Qs',
  authDomain: 'aptonoapto-c32b5.firebaseapp.com',
  databaseURL: 'https://aptonoapto-c32b5.firebaseio.com',
  projectId: 'aptonoapto-c32b5',
  storageBucket: 'aptonoapto-c32b5.appspot.com',
  messagingSenderId: '210508628354',
  appId: '1:210508628354:web:53ef3b57fddf8afd798f9b',
  measurementId: 'G-2CWNNZB4NQ'
};

// Init our firebase app
firebase.initializeApp(firebaseConfig);
