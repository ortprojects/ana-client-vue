import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Home.vue'),
    meta: {
      authRequired: false
    }
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/SignUp.vue'),
    meta: {
      authRequired: false
    }
  },
  {
    path: '/posts',
    name: 'Posts',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Posts/Posts.vue'),
    meta: {
      authRequired: true
    }
  },
  {
    path: '/post/:postId',
    name: 'Post',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Posts/Post.vue'),
    props: true,
    meta: {
      authRequired: true
    }
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Admin.vue'),
    meta: {
      authRequired: true,
      adminRequired: true
    }
  },
  {
    path: '/user-profile',
    name: 'UserProfile',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/UserProfile.vue'),
    meta: {
      authRequired: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const accessToken = localStorage.getItem('accessToken');
  const userRolId = localStorage.getItem('userRolId');

  if (to.meta.authRequired && accessToken === null) {
    return next({
      path: '/'
    });
  }
  if (to.fullPath === '/admin' && userRolId !== '1') {
    return next({
      path: '/'
    });
  }
  return next();
});

export default router;
