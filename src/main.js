import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import '@/plugins/firebase';
import firebase from 'firebase/app';

Vue.config.productionTip = false;

firebase.auth().onAuthStateChanged(function(user) {
  const accessToken = localStorage.getItem('accessToken');
  if (user && accessToken) {
    store.dispatch('authModule/setAuthState', user);
  } else {
    // No user is signed in.
  }
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
