const snackBarMixin = {
  methods: {
    openSnackbar(snackbarOptions) {
      this.$store.dispatch(
        'componentModule/snackbarModule/openSnackbar',
        snackbarOptions
      );
    }
  }
};

export default snackBarMixin;
